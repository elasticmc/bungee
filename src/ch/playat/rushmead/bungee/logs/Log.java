package ch.playat.rushmead.bungee.logs;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.messaging.Messaging;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class Log {

    private static String lastError = "";

    public static void og(String message, Object... replacements) {
        og(Level.INFO, message, replacements);
    }

    public static void og(Level level, String message, Object... replacements) {

        System.out.println("[" + level.getTag() + "] " + Messaging.replace(message, replacements));
    }

    public static void ogAction(Level level, String player, String action, String details) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `logs_actions` (`level`, `server`, `player`, `action`, `details`) VALUES (?,?,?,?,?)");
            ps.setString(1, level.name());
            ps.setString(2, "bungee");
            ps.setString(3, player);
            ps.setString(4, action);
            ps.setString(5, details);
            ps.setString(6, "" + System.currentTimeMillis());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void ogError(Level level, String trigger, String description, String error) {
        og(level, description);

        if (!description.equals(lastError)) {
            lastError = description;
            try {
                Connection c = DatabaseConnections.getConnection();
                PreparedStatement ps = c.prepareStatement("INSERT INTO `logs_errors` (`level`, `server`, `error_trigger`, `description`, `error`, `timestamp`) VALUES (?,?,?,?,?,?)");
                ps.setString(1, level.name());
                ps.setString(2, "bungee");
                ps.setString(3, trigger.toLowerCase());
                ps.setString(4, description);
                ps.setString(5, error);
                ps.setString(6, "" + System.currentTimeMillis());
                ps.executeUpdate();
                ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void ogBroadcasts(String broadcaster, String broadcast) {
        og(broadcaster + " broadcasted " + broadcast);
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `logs_broadcasts` (`server`, `broadcaster`, `broadcast`, `timestamp`) VALUES (?,?,?,?)");
            ps.setString(1, "bungee");
            ps.setString(2, broadcaster);
            ps.setString(3, broadcast);
            ps.setString(4, "" + System.currentTimeMillis());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void ogKick(String kicker, String kicked, String reason) {
        og(kicker + " kicked " + kicked + " for " + reason);
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `logs_kicks` (`server`, `kicker`, `kicked`, `reason`, `timestamp`) VALUES (?,?,?,?,?)");
            ps.setString(1, "bungee");
            ps.setString(2, kicker);
            ps.setString(3, kicked);
            ps.setString(4, reason);
            ps.setString(5, "" + System.currentTimeMillis());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void ogRankChange(String setter, String set, int rank) {
        og(setter + " changed " + set + "'s rank to " + rank);
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `logs_ranks` (`setter`, `set`, `rank`, `timestamp`) VALUES (?,?,?,?)");
            ps.setString(1, setter);
            ps.setString(2, set);
            ps.setInt(3, rank);
            ps.setString(4, "" + System.currentTimeMillis());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
