package ch.playat.rushmead.bungee.partys;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rushmead for ElasticMC
 */
public class Party {


    private String partyLeader;
    private List<String> partyPlayers;
    private List<String> partyInvites;

    public Party(String partyLeader) {
        this.setPartyLeader(partyLeader);
        partyPlayers = new ArrayList<String>();
        partyInvites = new ArrayList<String>();
    }


    public String getPartyLeader() {
        return partyLeader;
    }

    public void setPartyLeader(String partyLeader) {
        this.partyLeader = partyLeader;
    }

    public List<String> getPartyPlayers() {
        return partyPlayers;
    }

    public void addPartyPlayer(String player) {
        this.partyPlayers.add(player);
    }

    public List<String> getPartyInvites() {
        return partyInvites;
    }

    public void addPartyInvite(String player) {
        this.partyInvites.add(player);
    }
}
