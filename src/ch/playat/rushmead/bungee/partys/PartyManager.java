package ch.playat.rushmead.bungee.partys;

import java.util.HashMap;

/**
 * Created by Rushmead for ElasticMC
 */
public class PartyManager {

    private static HashMap<String, Party> parties = new HashMap<>();

    public static HashMap<String,Party> getParties() {
        return parties;
    }

    public static Party newParty(String leader) {
        Party p =  new Party(leader);
        parties.put(leader, p);
        return p;
    }
}
