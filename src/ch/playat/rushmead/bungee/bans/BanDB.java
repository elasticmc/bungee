package ch.playat.rushmead.bungee.bans;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class BanDB {

    public synchronized void banPlayer(String banner, String banned, Offense offense, String evidence, boolean perm) {
        int points = 0;
        if (perm) {
            points = 10;
        } else {
            points = getBanPoints(banned);
            points = points + offense.getPoints();
        }
        String expiry = UsefullBanStuff.getBanEndTime(points) + "";
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `bans` (`banner`, `banned`, `points`, `reason`, `evidence`, `startdate`, `enddate`) VALUES (?,?,?,?,?,?,?)");
            ps.setString(1, banner);
            ps.setString(2, banned);
            ps.setInt(3, offense.getPoints());
            ps.setString(4, offense.getName());
            ps.setString(5, evidence);
            ps.setString(6, "" + (System.currentTimeMillis() / 1000));
            ps.setString(7, expiry);
            ps.executeUpdate();
            ps.close();

            PreparedStatement ps2 = c.prepareStatement("UPDATE `players` SET `player_banned`=?, `player_banexpiry`=?, `player_banpoints`=?, `player_banreason`=? WHERE `player_username`=?");
            ps2.setBoolean(1, true);
            ps2.setString(2, expiry);
            ps2.setInt(3, points);
            ps2.setString(4, offense.getName());
            ps2.setString(5, banned);
            ps2.executeUpdate();
            ps2.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void banPlayer(String banner, BungeePlayer banned, Offense offense, String evidence, boolean perm) {
        String expiry;
        if (perm) {
            expiry = UsefullBanStuff.getBanEndTime(10) + "";
        } else {
            expiry = UsefullBanStuff.getBanEndTime(banned.getBanPoints() + offense.getPoints()) + "";
        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `bans` (`banner`, `banned`, `points`, `reason`, `evidence`, `startdate`, `enddate`) VALUES (?,?,?,?,?,?,?)");
            ps.setString(1, banner);
            ps.setString(2, banned.getName());
            ps.setInt(3, offense.getPoints());
            ps.setString(4, offense.getName());
            ps.setString(5, "");
            ps.setString(6, "" + (System.currentTimeMillis() / 1000));
            ps.setString(7, expiry);
            ps.executeUpdate();
            ps.close();

            PreparedStatement ps2 = c.prepareStatement("UPDATE `players` SET `player_banned`=?, `player_banexpiry`=?, `player_banpoints`=?, `player_banreason`=? WHERE `player_username`=?");
            ps2.setBoolean(1, true);
            ps2.setString(2, expiry);
            ps2.setInt(3, banned.getBanPoints());
            ps2.setString(4, offense.getName());
            ps2.setString(5, banned.getName());
            ps2.executeUpdate();
            ps2.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized boolean hasBanExpired(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` =?");
            ps.setString(1, name);
            ResultSet results = ps.executeQuery();
            boolean hasAccount = results.next();

            if (hasAccount) {
                if (results.getBoolean("player_banned") == false) {
                    return true;
                }
                long time = Long.parseLong(results.getString("player_banexpiry"));
                long now = System.currentTimeMillis() / 1000;

                if (time > 0 && now >= time) {
                    return true;
                }
            }
            results.close();
            ps.close();
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return true;
        }
    }

    public synchronized int getBanPoints(String name) {
        try {
            return BungeePlayer.getPlayer((name.toLowerCase())).getBanPoints();

        } catch (NullPointerException e) {

        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username`=?");
            ps.setString(1, name);
            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getInt("player_banpoints");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public synchronized void unbanPlayer(BungeePlayer unbanned) {
        unbanned.setBanPoints(unbanned.getBanPoints() - unbanned.getLastBanOffense().getPoints());
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_banned`=?, `player_banexpiry`=?, `player_banpoints`=?, `player_banreason`=? WHERE `player_username`=?");
            ps.setBoolean(1, false);
            ps.setString(2, "0");
            ps.setInt(3, unbanned.getBanPoints());
            ps.setString(4, "None");
            ps.setString(5, unbanned.getName());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void unbanPlayer(String unbanned) {

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_banned`=?, `player_banexpiry`=?, `player_banreason`=? WHERE `player_username`=?");
            ps.setBoolean(1, false);
            ps.setString(2, "0");
            ps.setString(3, "None");
            ps.setString(4, unbanned);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
