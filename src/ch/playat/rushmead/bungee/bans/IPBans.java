package ch.playat.rushmead.bungee.bans;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rushmead
 */
public class IPBans {

    private static ArrayList<String> ips = new ArrayList<>();

    public static ArrayList<String> getIps() {
        return ips;
    }

    public static boolean isBannedIP(String ip) {
        return ips.contains(ip);
    }

    public synchronized static void addIPBan(String ip, String username, String banner, String reason) {
        if (isBannedIP(ip)) {
            return;
        }

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `bans_ips` (`banner`, `ip`, `names`, `reason`, `timestamp`) VALUES (?,?,?,?,?)");
            ps.setString(1, banner);
            ps.setString(2, ip);
            ps.setString(3, ip);
            ps.setString(4, username);
            ps.setString(5, "" + System.currentTimeMillis() / 1000);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        ips.add(ip);
    }

    public synchronized static void updateIPBan(String ip, String username) {
        String names = "", oldNames = "";
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `bans_ips` WHERE `ip`=?");
            ps.setString(1, ip);
            ResultSet results = ps.executeQuery();

            if (results.next()) {
                names = results.getString("names");
                oldNames = results.getString("names");
            } else {

            }
            results.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (names == null || names == "") {
            return;
        }
        boolean contains = false;

        String[] nameArray = names.split(",");
        for (String name : nameArray) {
            if (name.equalsIgnoreCase(username)) {
                contains = true;
            }
        }
        if (!contains) {
            names += username = ",";
        }
        if (oldNames.equalsIgnoreCase(names)) {
            return;
        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `bans_ips` SET `names`=? WHERE `ip`=?");
            ps.setString(1, names);
            ps.setString(2, ip);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void reloadIPBans() {
        ips.clear();

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `ip` FROM `bans_ips`");
            ResultSet results = ps.executeQuery();

            while (results.next()) {
                ips.add(results.getString("ip"));
            }
            results.close();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
