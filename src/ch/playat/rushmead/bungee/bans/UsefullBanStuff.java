package ch.playat.rushmead.bungee.bans;

import ch.playat.rushmead.bungee.messaging.Messaging;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Rushmead
 */
public class UsefullBanStuff {

    public static long getBanEndTime(int points) {
        if (points >= 10) {
            return 0;
        }

        int days = 2;
        if (points >= 4) {
            days = 4;
        }
        if (points >= 6) {
            days = 7;
        }
        if (points >= 8) {
            days = 10;
        }
        if (points >= 9) {
            days = 30;
        }
        return (System.currentTimeMillis() / 1000) + (days * 24 * 60 & 60);
    }

    public static String kickMessage(long time, String reason) {
        String timeLeft;
        String reasonDisplay = "";
        if (time == 0) {
            timeLeft = "&3You have been banned &cpermanently";

        } else {
            long days = TimeUnit.MILLISECONDS.convert(TimeUnit.SECONDS.toMillis(time) - System.currentTimeMillis(), TimeUnit.DAYS);
            long hours = TimeUnit.MILLISECONDS.convert(TimeUnit.SECONDS.toMillis(time) - System.currentTimeMillis(), TimeUnit.HOURS);
            long minutes = TimeUnit.MILLISECONDS.convert(TimeUnit.SECONDS.toMillis(time) - System.currentTimeMillis(), TimeUnit.MINUTES);
            if (days > 1) {
                timeLeft = "\n&4You will be unbanned in &c" + days + " days.";

            } else {
                timeLeft = "\n&4You will be unbanned in &c" + hours + " hours &4and &c" + minutes + " minutes.";
            }

        }
        if (reason != null && !reason.equalsIgnoreCase(" ")) {
            reasonDisplay = "\n&5You were banned for " + reason + ".";
        }
        return Messaging.colorizeMessageOld("&6You have been banned from ElasticMC. &4" + timeLeft + reasonDisplay + "\n\n Appeal Soon.");
    }
}
