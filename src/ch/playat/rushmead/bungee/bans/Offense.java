/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.playat.rushmead.bungee.bans;

import ch.playat.rushmead.bungee.ranks.PermissionSet;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Stuart
 */
public enum Offense {

    NONE(PermissionSet.ALL, "Nothing", 0, Arrays.asList("nothing")),
    BAN_EVASION(PermissionSet.SNR_STAFF, "Ban Evasion", 10, Arrays.asList("evasion", "banevasion")),
    SPAM(PermissionSet.JNR_STAFF, "Spam", 3, Arrays.asList("spam", "spamming"));

    private PermissionSet perms;

    private String name;
    private int points;
    private List<String> aliases;

    private Offense(PermissionSet perms, String name, int points, List<String> aliases) {
        this.perms = perms;
        this.name = name;
        this.points = points;
        this.aliases = aliases;
    }

    public PermissionSet getMinimumPermissions() {
        return perms;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public static Offense getOffense(String offesnse) {
        for (Offense p : Offense.values()) {
            if (p.getName().equalsIgnoreCase(offesnse)) {
                return p;
            }
        }
        for (Offense o : Offense.values()) {
            for (String oName : o.getAliases()) {
                if (oName.equalsIgnoreCase(offesnse)) {
                    return o;
                }
            }
        }
        return null;
    }
}
