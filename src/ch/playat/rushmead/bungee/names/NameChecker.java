package ch.playat.rushmead.bungee.names;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.messaging.Messaging;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rushmead
 */
public class NameChecker {

    private static ArrayList<String> names = new ArrayList<>();

    public static ArrayList<String> getNames() {
        return names;
    }

    public static boolean hasAlreadyWarned(String username) {
        return names.contains(username);
    }

    public synchronized static void tryAddNameWarning(String username, Names name) {
        if (name.getSeverity() == 0 || hasAlreadyWarned(username)) {
            return;
        }

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `warnings` (`username`, `severity`) VALUES (?,?)");
            ps.setString(1, username);
            ps.setInt(2, name.getSeverity());
            ps.executeUpdate();
            ps.close();
            names.add(username);
            Messaging.sendStaffMessage("{0} has been detected as possible offense name (Level {1}. Please Go to the Staff Panel to see more", username, name.getSeverity());

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public synchronized static void reloadNameWarnings() {
        names.clear();

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `warnings`");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                names.add(rs.getString("username"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void checkName(String username) {
        Names warning = Names.CLEAN;

        for (String word : Names.MILD.getWords()) {
            if (username.toLowerCase().contains((word.toLowerCase()))) {
                warning = warning = Names.MILD;
            }
        }

        for (String word : Names.MEDIUM.getWords()) {
            if (username.toLowerCase().contains((word.toLowerCase()))) {
                warning = warning = Names.MEDIUM;
            }
        }

        for (String word : Names.SEVERE.getWords()) {
            if (username.toLowerCase().contains((word.toLowerCase()))) {
                warning = warning = Names.SEVERE;
            }
        }

        if (warning.getSeverity() > 0) {
            tryAddNameWarning(username, warning);
        }
    }
}
