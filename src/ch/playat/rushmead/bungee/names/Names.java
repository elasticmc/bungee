package ch.playat.rushmead.bungee.names;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Rushmead
 */
public enum Names {

    CLEAN(0, null),
    MILD(1, Arrays.asList("crap", "douche", "dick", "vagina", "anus", "butt", "asshole", "cum")),
    MEDIUM(2, Arrays.asList("shit", "fuck", "cock", "penis", "cunt", "whore", "porn", "anal")),
    SEVERE(3, Arrays.asList("nigg", "nigr", "nigger", "nigga", "niglet", "hitler", "suicid", "genocid", "rape"));

    private int severity;
    private List<String> words;

    Names(int severity, List<String> words) {
        this.severity = severity;
        this.words = words;

    }

    public int getSeverity() {
        return severity;
    }

    public List<String> getWords() {
        return words;
    }
}
