package ch.playat.rushmead.bungee.messaging;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.partys.Party;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author Rushmead
 */
public class Messaging {

    private static String _prefix = colorizeMessageOld("&3Elastic&5&lMC&f | ");

    public static String getPrefix() {
        return _prefix;
    }

    public static TextComponent colorizeMessage(String message) {
        TextComponent tx = new TextComponent(TextComponent.fromLegacyText(message));
        return tx;
    }

    public static String colorizeMessageOld(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static void sendClickableAndHoveringMessage(ProxiedPlayer player, String message, String hover, String command) {
        TextComponent textComponent = new TextComponent(getPrefix() + colorizeMessageOld(message));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(colorizeMessageOld(hover)).create()));
        player.sendMessage(textComponent);
    }

    public static void broadcastMessage(String message, Object... replacements) {
        ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText(getPrefix() + replace(message, replacements)));
    }

    public static void sendMessage(ProxiedPlayer p, String message, Object... replacements) {
        p.sendMessage(TextComponent.fromLegacyText(getPrefix() + replace(message, replacements)));
    }

    public static void sendMessage(CommandSender sender, String message, Object... replacements) {
        sender.sendMessage(TextComponent.fromLegacyText(getPrefix() + replace(message, replacements)));
    }

    public static void broadcastRawMessage(String message, Object... replacements) {
<<<<<<< HEAD
        ProxyServer.getInstance().broadcast(replace(message, replacements));
=======
        ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText(replace(message, replacements)));
>>>>>>> 791d7571dea2517ffe9c5ba13c8de76ff58c3137
    }

    public static void sendRawMessage(ProxiedPlayer p, String message, Object... replacements) {
        p.sendMessage(TextComponent.fromLegacyText(replace(message, replacements)));
    }

    public static void sendRawMessage(CommandSender sender, String message, Object... replacements) {
        sender.sendMessage(TextComponent.fromLegacyText(replace(message, replacements)));
    }

    public static String getPrefix(String name) {
        return colorizeMessageOld("&a" + name + " &8\u00BB &7");
    }

    public static void sendStaffMessage(String message, Object... replacements) {
        TextComponent tx = new TextComponent(TextComponent.fromLegacyText(getPrefix("&5Staff") + replace(message, replacements)));
        for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
            for (ProxiedPlayer ps : server.getPlayers()) {
                try {
                    if (BungeePlayer.getPlayer(ps.getName().toLowerCase()).getPermissions().getPower() >= PermissionSet.JNR_STAFF.getPower()) {
                        ps.sendMessage(tx);
                    }
                } catch (NullPointerException e) {

                }
            }
        }
    }

    public static void sendRawStaffMessage(String message, Object... replacements) {
        TextComponent tx = new TextComponent(TextComponent.fromLegacyText(replace(message, replacements)));
        for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
            for (ProxiedPlayer ps : server.getPlayers()) {
                try {
                    if (BungeePlayer.getPlayer(ps.getName().toLowerCase()).getPermissions().getPower() >= PermissionSet.JNR_STAFF.getPower()) {
                        ps.sendMessage(tx);
                    }
                } catch (NullPointerException e) {

                }
            }
        }
    }

    public static void sendRawPartyMessage(Party party, String message, Object... replacements) {
        TextComponent tx = new TextComponent(TextComponent.fromLegacyText(replace(message, replacements)));
        if (Bungeecord.getInstance().getProxy().getPlayer(party.getPartyLeader()) != null) {
            Bungeecord.getInstance().getProxy().getPlayer(party.getPartyLeader()).sendMessage(tx);
        }
        for (String s : party.getPartyPlayers()) {
            if (Bungeecord.getInstance().getProxy().getPlayer(s) != null) {
                Bungeecord.getInstance().getProxy().getPlayer(s).sendMessage(tx);
            }
        }
    }

    public static void sendPartyMessage(Party party, String message, Object... replacements) {
        TextComponent tx = new TextComponent(TextComponent.fromLegacyText(getPrefix("&5Party") + replace(message, replacements)));
        if (Bungeecord.getInstance().getProxy().getPlayer(party.getPartyLeader()) != null) {
            Bungeecord.getInstance().getProxy().getPlayer(party.getPartyLeader()).sendMessage(tx);
        }
        for (String s : party.getPartyPlayers()) {
            if (Bungeecord.getInstance().getProxy().getPlayer(s) != null) {
                Bungeecord.getInstance().getProxy().getPlayer(s).sendMessage(tx);
            }
        }
    }

    public static String replace(String msg, Object... replacements) {
        if (replacements.length > 0) {

            for (int i = 0; i < replacements.length; i++) {
                if (msg.contains("{" + i + "}")) {
                    msg = msg.replace("{" + i + "}", replacements[i].toString());
                } else {
                    msg = msg.replace("{" + i + "}", "");
                }
            }
        }
        return colorizeMessageOld(msg);
    }
}
