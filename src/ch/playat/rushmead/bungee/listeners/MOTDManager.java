package ch.playat.rushmead.bungee.listeners;

import ch.playat.rushmead.bungee.messaging.Messaging;
import jline.internal.InputStreamReader;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.BufferedReader;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Rushmead
 */
public class MOTDManager implements Listener {

    @EventHandler
    public void onMOTD(ProxyPingEvent e) {
        ServerPing ping = e.getResponse();
        try {
            URL u = new URL("http://rushmead.playat.ch:8080/motd/");
            URLConnection c = u.openConnection();
            InputStream r = c.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(r));
            String motd = reader.readLine();
            ping.setDescription(Messaging.colorizeMessageOld(motd));
        } catch (Exception es) {
            es.printStackTrace();
        }
        ping.getPlayers().setMax(10000);
        
      
        e.setResponse(ping);
    }
}
