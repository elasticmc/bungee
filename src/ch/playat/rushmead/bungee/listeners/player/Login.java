package ch.playat.rushmead.bungee.listeners.player;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.bans.Bans;
import ch.playat.rushmead.bungee.bans.IPBans;
import ch.playat.rushmead.bungee.bans.Offense;
import ch.playat.rushmead.bungee.bans.UsefullBanStuff;
import ch.playat.rushmead.bungee.logs.Level;
import ch.playat.rushmead.bungee.logs.Log;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.players.BungeePlayerDB;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 *
 * @author Rushmead
 */
public class Login implements Listener {

    @EventHandler
    public void onPlayerLogin(LoginEvent e) {
       
        try {
            String[] banData = BungeePlayerDB.getDatas().getBanDataFromUUID(e.getConnection().getName(), e.getConnection().getUniqueId().toString());
            if (banData[0] == "1" || banData[0].equalsIgnoreCase("true")) {
                if (banData[1].equalsIgnoreCase("0")) {
                    IPBans.addIPBan(e.getConnection().getAddress().getAddress().getHostAddress(), e.getConnection().getName(), "CONSOLE", "Ban evasion");
                }

                if (Long.parseLong(banData[1]) != 0 && (System.currentTimeMillis() / 1000) > Long.parseLong(banData[1])) {
                    Bans.getBanDB().unbanPlayer(e.getConnection().getName());
                    //TODO Log He has been unbanned? MAYBE
                } else {
                    e.setCancelReason(UsefullBanStuff.kickMessage(Long.parseLong(banData[1]), banData[2]));
                    e.setCancelled(true);
                    //TODO Log
                    return;
                }
            }

        } catch (NullPointerException es) {
            if (BungeePlayerDB.getDatas().isInDatabase(e.getConnection().getName())) {
                String error = es.getMessage();
                if (error == null) {
                    error = "No extra information given.";
                }
                Log.ogError(Level.CRITICAL, "Ban info", "Failed to get " + e.getConnection().getName() + "'s ban info", error);
            }
        }
        if (IPBans.isBannedIP(e.getConnection().getAddress().getAddress().getHostAddress())) {
            e.setCancelled(true);
            e.setCancelReason(UsefullBanStuff.kickMessage(0, ""));

            IPBans.updateIPBan(e.getConnection().getAddress().getAddress().getHostAddress(), e.getConnection().getName());
            if (!Bans.getBanDB().hasBanExpired(e.getConnection().getName())) {
                Bans.getBanDB().banPlayer("CONSOLE", e.getConnection().getName(), Offense.BAN_EVASION, "Ban evasion. The banned IP they tried to use was " + e.getConnection().getAddress().getAddress().getHostAddress(), true);
            }
            return;
        }
        if(!Bungeecord.getInstance().isPublic){
            if(BungeePlayerDB.getDatas().isInDatabase(e.getConnection().getName())){
                if(BungeePlayerDB.getDatas().getPlayer(e.getConnection().getName(),e.getConnection().getUUID()).getPermissions().getPower() <= PermissionSet.JNR_STAFF.getPower()){
                    e.setCancelled(true);
                    e.setCancelReason(Messaging.colorizeMessageOld(Messaging.getPrefix() + "&aWe are coming soon... Please wait <3"));
                }
            }
        }
        BungeePlayer.addPlayer(e.getConnection().getName(), e.getConnection().getUniqueId().toString());

    }
}
