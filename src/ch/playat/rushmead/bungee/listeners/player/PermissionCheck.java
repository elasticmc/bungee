package ch.playat.rushmead.bungee.listeners.player;

import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
/*
 *
 * @author Rushmead
 */

public class PermissionCheck implements Listener {
    
    @EventHandler
    public void onEventPerm(PermissionCheckEvent e) {
        if (e.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) e.getSender();
            if (BungeePlayer.getPlayer(p.getName().toLowerCase()).getPermissions().getPower() >= PermissionSet.SNR_STAFF.getPower()) {
                e.setHasPermission(true);
            }
        }
        
    }
}
