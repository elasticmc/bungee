package ch.playat.rushmead.bungee.listeners.player;

import ch.playat.rushmead.bungee.names.NameChecker;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.players.BungeePlayerDB;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 *
 * @author Rushmead
 */
public class Join implements Listener {

    @EventHandler
    public void onPlayerJoin(PostLoginEvent e) {
        ProxiedPlayer p = e.getPlayer();
        NameChecker.checkName(p.getName());
       
        BungeePlayerDB.getDatas().savePlayerToDB(p, -1);

    }

}
