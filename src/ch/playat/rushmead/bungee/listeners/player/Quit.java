package ch.playat.rushmead.bungee.listeners.player;

import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author Rushmead
 */
public class Quit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerDisconnectEvent e) {
        if (BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).isInParty()) {
            if (BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty().getPartyLeader() == e.getPlayer().getName().toLowerCase()) {
                BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty().setPartyLeader(BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty().getPartyPlayers().get(0));
                Messaging.sendPartyMessage(BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty(), "&5{0} has left, {1} is the new party leader", e.getPlayer()
                        .getName(), BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty().getPartyLeader());
            } else {
                Messaging.sendPartyMessage(BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty(), "&5{0} has left", e.getPlayer().getName());
                BungeePlayer.getPlayer(e.getPlayer().getName().toLowerCase()).getParty().getPartyPlayers().remove(e.getPlayer().getDisplayName().toLowerCase());
            }
        }
        BungeePlayer.removePlayer(e.getPlayer());
    }

}
