package ch.playat.rushmead.bungee.listeners.messaging;

import ch.playat.rushmead.bungee.Bungeecord;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 *
 * @author Rushmead
 */
public class Messaging_ServerSlack implements Listener {

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        if (!e.getTag().equalsIgnoreCase("BungeeCord")) {
            return;
        }
        if (!(e.getSender() instanceof Server)) {
            return;
        }

        ByteArrayInputStream stream = new ByteArrayInputStream(e.getData());
        DataInputStream in = new DataInputStream(stream);

        try {
            if (!in.readUTF().equalsIgnoreCase("slack")) {
                return;
            }
        } catch (IOException es) {

        }
        try{
            if(in.readUTF() == "online"){
                 String server = in.readUTF();
            Bungeecord.getInstance().session.sendMessage(Bungeecord.getInstance().session.findChannelByName("serverstatus"), "["+ server + "] Has just come online.", null);
            }else if(in.readUTF() == "offline"){
                String server = in.readUTF();
            Bungeecord.getInstance().session.sendMessage(Bungeecord.getInstance().session.findChannelByName("serverstatus"), "["+ server + "] Has just gone offline.", null);
            }
           
        }catch(NumberFormatException | IOException ex){
         ex.printStackTrace();
        }catch(NullPointerException ex){
            
        }
    }
}
