

package ch.playat.rushmead.bungee.listeners.messaging;

import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.Ranks;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 *
 * @author Rushmead
 */
public class Messaging_RankSet implements Listener
{
    @EventHandler
    public void onPluginMessage(PluginMessageEvent e)
    {
        if (!e.getTag().equalsIgnoreCase("BungeeCord")) {
            return;
        }
        if (!(e.getSender() instanceof Server)) {
            return;
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(e.getData());
        DataInputStream in = new DataInputStream(stream);

        try {
            if (!in.readUTF().equalsIgnoreCase("rankset")) {
                return;
            }
        }
        catch (IOException ex) {
        }

        try {
            String name = in.readUTF();
            int rank = in.readInt();
            BungeePlayer.getPlayer(name).setRank(Ranks.getRanks().getRankFromID(rank));
        }
        catch (NumberFormatException | IOException ex) {
            ex.printStackTrace();
        }
        catch (NullPointerException ex) {
        }
    }

}