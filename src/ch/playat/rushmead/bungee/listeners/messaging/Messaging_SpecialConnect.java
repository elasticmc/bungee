package ch.playat.rushmead.bungee.listeners.messaging;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.*;

/**
 * Created by Rushmead for ElasticMC
 */
public class Messaging_SpecialConnect implements Listener {
    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        if (!e.getTag().equalsIgnoreCase("BungeeCord")) {
            return;
        }
        if (!(e.getSender() instanceof Server)) {
            return;
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(e.getData());
        DataInputStream in = new DataInputStream(stream);

        try {
            if (!in.readUTF().equalsIgnoreCase("SpecialConnect")) {
                return;
            }
        } catch (IOException ex) {
        }

        try {
            String name = in.readUTF();
            String server = in.readUTF();
            if (BungeePlayer.getPlayer(name.toLowerCase()).isInParty()) {
                if (BungeePlayer.getPlayer(name.toLowerCase()).getParty().getPartyLeader().equals(name.toLowerCase())) {
                    final ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);

                    try {
                        out.writeUTF("Connect");
                        out.writeUTF(server);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    Bungeecord.getInstance().getProxy().getPlayer(name).sendData("BungeeCord", b.toByteArray());
                    for (String s : BungeePlayer.getPlayer(name.toLowerCase()).getParty().getPartyPlayers()) {
                        Bungeecord.getInstance().getProxy().getPlayer(s).sendData("BungeeCord", b.toByteArray());
                    }
                }
            }
        } catch (NumberFormatException | IOException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
        }
    }

}