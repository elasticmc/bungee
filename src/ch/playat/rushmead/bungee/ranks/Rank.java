package ch.playat.rushmead.bungee.ranks;

import net.md_5.bungee.api.ChatColor;

/**
 *
 * @author Rushmead
 */
public enum Rank {

   OWNER(10, "Owner", PermissionSet.ALL, ChatColor.GOLD, ChatColor.BOLD),
    HEAD_ADMIN(9, "Head Admin", PermissionSet.ALL, ChatColor.GOLD),
    ADMIN(8, "Admin", PermissionSet.SNR_STAFF, ChatColor.RED),
    MODERATOR(7, "Moderator", PermissionSet.STAFF, ChatColor.DARK_PURPLE),
    HELPER(6, "Helper", PermissionSet.JNR_STAFF, ChatColor.LIGHT_PURPLE),
    CREEPERHOSTSTAFF(5, "CreeperHost Staff", PermissionSet.MIB_PLUS, ChatColor.DARK_GREEN),
    MIB_PLUS(4, "MVB+", PermissionSet.MIB_PLUS, ChatColor.YELLOW),
    MIB(3, "MVB", PermissionSet.MIB, ChatColor.DARK_GREEN),
    VIB_PLUS(2, "VIB+", PermissionSet.VIB_PLUS, ChatColor.AQUA),
    VIB(1, "VIB", PermissionSet.VIB, ChatColor.BLUE),
    REGULAR(0, "Regular", PermissionSet.DEFAULT, ChatColor.GRAY);
    private int id;

    private String rankName;
    private ChatColor rankColor;
    private ChatColor rankColor1;
    private PermissionSet rankPermissions;

    Rank(int power, String rankName, PermissionSet rankPermissions, ChatColor... colors) {
        this.id = power;
        this.rankName = rankName;
        this.rankColor = colors[0];
        if (colors.length == 2) {
            this.rankColor1 = colors[1];
        }
        this.rankPermissions = rankPermissions;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return rankName;
    }

    public ChatColor getColor() {
        return rankColor;
    }

    public boolean hasTwoColours() {
        if(rankColor1 == null){
            return false;
        }
        return true;
    }
    public ChatColor getColor2(){
        return rankColor1;
    }

    public String getPrefix() {
        if (rankName.equals(Rank.REGULAR.getName())) {
            return rankColor + "";
        } else {
            return rankColor + "" + ChatColor.BOLD + rankName + ChatColor.WHITE + " | " + rankColor;
        }
    }

    public PermissionSet getPermissionSet() {
        return rankPermissions;

    }   
}
