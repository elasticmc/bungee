package ch.playat.rushmead.bungee.ranks;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.players.BungeePlayerDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class Ranks {

    private static Ranks r = new Ranks();

    public static Ranks getRanks() {
        return r;
    }

    public Rank getRankFromID(int id) {
          switch (id) {
            case 10:
                return Rank.OWNER;
            case 9:
                return Rank.HEAD_ADMIN;
            case 8:
                return Rank.ADMIN;
            case 7:
                return Rank.MODERATOR;
            case 6:
                return Rank.HELPER;
            case 5:
                return Rank.CREEPERHOSTSTAFF;
            case 4:
                return Rank.MIB_PLUS;
            case 3:
                return Rank.MIB;
            case 1:
                return Rank.VIB;
            case 2:
                return Rank.VIB_PLUS;
        }
        return Rank.REGULAR;
    }

    public synchronized Rank getRankFromDatabase(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);
            ResultSet result = ps.executeQuery();
            boolean hasRank = result.next();
            if (!hasRank) {
                result.close();
                ps.close();
                return Rank.REGULAR;
            } else {
                return getRankFromID(result.getInt("player_rank"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Rank.REGULAR;
    }

    public synchronized void setRank(String username, int rank) {
        if (!BungeePlayerDB.getDatas().isInDatabase(username)) {
            BungeePlayerDB.getDatas().addPlayer(username, "");
        }

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_rank` =? WHERE `player_username`= ?");
            ps.setInt(1, rank);
            ps.setString(2, username.toLowerCase());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
