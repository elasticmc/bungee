package ch.playat.rushmead.bungee.ranks;

/**
 *
 * @author Rushmead
 */
public enum PermissionSet {

     ALL(8),
    SNR_STAFF(7),
    STAFF(6),
    JNR_STAFF(5),
    MIB_PLUS(4),
    MIB(3),
    VIB_PLUS(2),
    VIB(1),
    DEFAULT(0);
    private int power;

    PermissionSet(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public static PermissionSet getSetFromPower(int power) {
        switch (power) {
            case 1:
                return PermissionSet.VIB;
            case 2:
                return PermissionSet.VIB_PLUS;
            case 3:
                return PermissionSet.MIB;
            case 4:
                return PermissionSet.MIB_PLUS;
            case 5:
                return PermissionSet.JNR_STAFF;
            case 6:
                return PermissionSet.STAFF;
            case 7:
                return PermissionSet.SNR_STAFF;
            case 8:
                return PermissionSet.ALL;
            case 0:
                return PermissionSet.DEFAULT;

        }
        return PermissionSet.DEFAULT;
    }

}
