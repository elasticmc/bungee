package ch.playat.rushmead.bungee;

import ch.playat.rushmead.bungee.bans.IPBans;
import ch.playat.rushmead.bungee.commands.admin.AnnounceCommand;
import ch.playat.rushmead.bungee.commands.admin.TogglePublic;
import ch.playat.rushmead.bungee.commands.admin.WhoIsCommand;
import ch.playat.rushmead.bungee.commands.mod.BanCommand;
import ch.playat.rushmead.bungee.commands.mod.StaffCommand;
import ch.playat.rushmead.bungee.commands.tp.HubCommand;
import ch.playat.rushmead.bungee.commands.user.Friends;
import ch.playat.rushmead.bungee.commands.user.PartiesCommand;
import ch.playat.rushmead.bungee.commands.user.WhereAmICommand;
import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.listeners.MOTDManager;
import ch.playat.rushmead.bungee.listeners.messaging.Messaging_PermissionSet;
import ch.playat.rushmead.bungee.listeners.messaging.Messaging_RankSet;
import ch.playat.rushmead.bungee.listeners.messaging.Messaging_ServerSlack;
import ch.playat.rushmead.bungee.listeners.messaging.Messaging_SpecialConnect;
import ch.playat.rushmead.bungee.listeners.player.*;
import ch.playat.rushmead.bungee.logs.Log;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.names.NameChecker;
import ch.playat.rushmead.bungee.players.BungeePlayerDB;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;

/**
 * @author Rushmead
 */
public class Bungeecord extends Plugin {

    private static Bungeecord instance;

    public String name;
    public SlackSession session;
    public boolean isPublic = false;
    public static Bungeecord getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {

        this.instance = this;
        name = "Proxy 1";
        getLogger().info("ElasticMC Bungecord Core Has Loaded....");
        this.getProxy().getPluginManager().registerListener(this, new MOTDManager());
        this.getProxy().getPluginManager().registerListener(this, new Login());
        this.getProxy().getPluginManager().registerListener(this, new Quit());
        this.getProxy().getPluginManager().registerListener(this, new Join());
        this.getProxy().getPluginManager().registerListener(this, new PlayerChatEvent());
        this.getProxy().getPluginManager().registerListener(this, new Messaging_ServerSlack());
        this.getProxy().getPluginManager().registerListener(this, new Messaging_RankSet());
        this.getProxy().getPluginManager().registerListener(this, new Messaging_PermissionSet());
        this.getProxy().getPluginManager().registerListener(this, new Messaging_SpecialConnect());
        this.getProxy().getPluginManager().registerListener(this, new PermissionCheck());
        this.getProxy().getPluginManager().registerCommand(this, new AnnounceCommand());
        this.getProxy().getPluginManager().registerCommand(this, new BanCommand());
        this.getProxy().getPluginManager().registerCommand(this, new HubCommand());
        this.getProxy().getPluginManager().registerCommand(this, new WhereAmICommand());
        this.getProxy().getPluginManager().registerCommand(this, new WhoIsCommand());
        this.getProxy().getPluginManager().registerCommand(this, new StaffCommand());
        this.getProxy().getPluginManager().registerCommand(this, new Friends());
        this.getProxy().getPluginManager().registerCommand(this, new PartiesCommand());
        this.getProxy().getPluginManager().registerCommand(this, new TogglePublic());
        DatabaseConnections.openConnection();
        IPBans.reloadIPBans();
        NameChecker.reloadNameWarnings();

        session = SlackSessionFactory.createWebSocketSlackSession("xoxb-11005727360-tEYCOkYSReT3TGYkjmO0cBdm");

        try {
            session.connect();

            session.sendMessage(session.findChannelByName("general"), "[" + name + "] has just come online.", null);
            session.addMessagePostedListener(new SlackMessagePostedListener() {
                @Override
                public void onEvent(SlackMessagePosted event, SlackSession session) {
                    if (event.getChannel().getName().equals("staffchat") && !event.getSender().getUserName().equalsIgnoreCase("mcbot")) {
                        Log.og("{0}", event.getSender().getUserName());
                        Messaging.sendStaffMessage(ChatColor.YELLOW + "Slack &8\u23aa " + event.getSender().getUserName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + event.getMessageContent());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        session.sendMessage(session.findChannelByName("general"), "[" + name + "] has just gone offline.", null);
        try {
            session.disconnect();
        } catch (IOException e) {

        }
        for (ProxiedPlayer ps : ProxyServer.getInstance().getPlayers()) {
            try {
                if (BungeePlayerDB.getDatas().isInDatabase(ps.getName())) {
                    BungeePlayerDB.getDatas().updateUUIDFromName(ps.getName(), ps.getUniqueId().toString());
                    BungeePlayerDB.getDatas().savePlayerToDB(ps, -1);
                } else {
                    BungeePlayerDB.getDatas().addPlayer(ps);
                }
            } catch (Exception ex) {
            }
        }
        DatabaseConnections.closeConnection();

    }
}
