package ch.playat.rushmead.bungee.players;

import ch.playat.rushmead.bungee.bans.Offense;
import ch.playat.rushmead.bungee.partys.Party;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import ch.playat.rushmead.bungee.ranks.Rank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Rushmead
 */
public class BungeePlayer {

    private static HashMap<String, BungeePlayer> players = new HashMap<>();

    public static HashMap<String, BungeePlayer> getPlayers() {
        return players;

    }

    public static BungeePlayer getPlayer(String name) {
        return players.get(name);
    }

    public static void addPlayer(String name, String uuid) {
        name = name.toLowerCase();
        if (BungeePlayer.players.containsKey(name)) {
            return;
        }
        BungeePlayer bp = BungeePlayerDB.getDatas().getPlayer(name, uuid);
        if (bp == null) {
            bp = BungeePlayerDB.getDatas().getPlayer(name, uuid);
        }
        if (bp == null) {
            bp = new BungeePlayer(name, uuid, Rank.REGULAR, PermissionSet.DEFAULT, "None", "HUB1", "0", 0, "-1,", "-1", true);
        }

        BungeePlayer.players.put(name.toLowerCase(), bp);
    }

    public static void removePlayer(ProxiedPlayer p) {
        if (!BungeePlayer.players.containsKey(p.getName().toLowerCase())) {
            return;
        }
        BungeePlayerDB.getDatas().updateUUIDFromName(p.getName().toLowerCase(), p.getUniqueId().toString());
        BungeePlayerDB.getDatas().savePlayerToDB(p, BungeePlayer.players.get(p.getName().toLowerCase()).getPlayTime());
        BungeePlayer.players.remove(p.getName().toLowerCase());
    }

    private String name;
    private String uuid;
    private Rank rank;
    private PermissionSet permissions;
    private Offense lastban;
    private String lastServer;
    private int banPoints;
    private long loginTime;
    private long playTime;
    private List<String> friends;
    private boolean friendRequestsEnabled;
    private List<String> friendRequests;
    private Party party;

    public BungeePlayer(String name, String uuid, Rank rank, PermissionSet permissions, String lastBan, String lastServer, String playTime, int banPoints, String friendArray, String friendRequestArray, boolean friendRequests) {
        this.name = name;
        this.uuid = uuid;
        this.rank = rank;
        this.permissions = permissions;
        this.banPoints = banPoints;
        this.lastServer = lastServer;
        this.loginTime = System.currentTimeMillis() / 1000;

        if (lastBan == null || lastBan.isEmpty()) {
            this.lastban = Offense.NONE;
        } else {
            this.lastban = Offense.getOffense(lastBan);
        }
        try {
            this.playTime = Long.parseLong(playTime);
        } catch (Exception e) {
            this.playTime = 0;
        }
        this.friends = getFriendsFromString(friendArray);
        this.friendRequests = getFriendsFromString(friendRequestArray);
        this.friendRequestsEnabled = friendRequests;
        this.party = null;
    }

    public ProxiedPlayer getPlayer() {
        return ProxyServer.getInstance().getPlayer(getName());
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @return the rank
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public int getElasticBands() {
        return BungeePlayerDB.getDatas().getElasticBands(name);
    }

    public void setElasticBands(int bands) {
        BungeePlayerDB.getDatas().setElasticBands(name, bands);
    }

    /**
     * @return the permissions
     */
    public PermissionSet getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(PermissionSet permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the lastServer
     */
    public String getLastServer() {
        return lastServer;
    }

    /**
     * @return the banPoints
     */
    public int getBanPoints() {
        return banPoints;
    }

    /**
     * @param banPoints the banPoints to set
     */
    public void setBanPoints(int banPoints) {
        this.banPoints = banPoints;
    }

    /**
     * @return the loginTime
     */
    public long getLoginTime() {
        return loginTime;
    }

    /**
     * @param loginTime the loginTime to set
     */
    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * @return the playTime
     */
    public long getPlayTime() {
        return playTime;
    }

    public Offense getLastBanOffense() {
        return lastban;
    }

    public List<String> getFriends() {
        return friends;
    }

    public String getFriendString() {
        String unlocked = "";

        for (String friend : friends) {

            unlocked += BungeePlayerDB.getDatas().getPlayerID(friend) + ",";

        }

        return unlocked;
    }

    public static List<String> getFriendsFromString(String friends) {
        if (friends.isEmpty()) {
            return new ArrayList<String>();
        }
        String[] toyStrs = friends.split(",");

        List<String> unlockedToys = new ArrayList<String>();

        for (String toyStr : toyStrs) {
            if (toyStr == "-1") {
                unlockedToys.add(" ");
                continue;
            }
            unlockedToys.add(BungeePlayerDB.getDatas().getNameFromID(Integer.parseInt(toyStr)));
        }

        return unlockedToys;
    }

    public boolean isFriendRequestsEnabled() {
        return friendRequestsEnabled;
    }

    public void setFriendRequestsEnabled(boolean friendRequestsEnabled) {
        this.friendRequestsEnabled = friendRequestsEnabled;
    }

    public List<String> getFriendRequests() {
        return friendRequests;
    }

    public String getFriendRequestString() {
        String unlocked = "";

        for (String friend : friendRequests) {

            unlocked += BungeePlayerDB.getDatas().getPlayerID(friend) + ",";

        }

        return unlocked;
    }

    public boolean isInParty() {
        return party != null;
    }
    public void setParty(Party p){
        this.party = p;
    }
    public Party getParty(){
        return party;
    }
}
