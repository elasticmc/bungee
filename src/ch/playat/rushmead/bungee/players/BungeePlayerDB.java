package ch.playat.rushmead.bungee.players;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import ch.playat.rushmead.bungee.ranks.Ranks;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Rushmead
 */
public class BungeePlayerDB {

    private static BungeePlayerDB data = new BungeePlayerDB();

    public static BungeePlayerDB getDatas() {
        return data;
    }

    public synchronized void addPlayer(String name, String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `players`(`player_username`, `player_uuid`, `player_firstLogin`, `player_lastLogin`, `player_friendsEnabled`) VALUES (?,?,?,?,?)");
            ps.setString(1, name);
            ps.setString(2, uuid);
            ps.setString(3, "" + System.currentTimeMillis() / 1000);
            ps.setString(4, "" + System.currentTimeMillis() / 1000);
            ps.setBoolean(5, true);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void addPlayer(ProxiedPlayer p) {

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `players`(`player_username`, `player_uuid`, `player_lastip`,`player_firstLogin`, `player_lastLogin`, `player_friendsEnabled`) VALUES (?,?,?,?,?,?)");
            ps.setString(1, p.getName());
            ps.setString(2, p.getUniqueId().toString());
            ps.setString(3, p.getAddress().getHostString());
            ps.setString(4, "" + System.currentTimeMillis() / 1000);
            ps.setString(5, "" + System.currentTimeMillis() / 1000);
            ps.setBoolean(5, true);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized boolean isInDatabase(String name) {
        try {

            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            boolean inDatabase = rs.next();
            return inDatabase;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized boolean isInDatabase(UUID uuid) {
        try {

            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_uuid` = ?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();

            boolean inDatabase = rs.next();
            return inDatabase;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized BungeePlayer getPlayer(String name, String uuid) {
        BungeePlayer player = null;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);
            ResultSet result = ps.executeQuery();
            boolean isAccount = result.next();

            if (isAccount) {
                if (result.getString("player_uuid") == null || result.getString("player_uuid").isEmpty()) {
                    updateUUIDFromName(name, uuid);
                }
                player = new BungeePlayer(name, uuid, Ranks.getRanks().getRankFromID(result.getInt("player_rank")), PermissionSet.getSetFromPower(result.getInt("player_permissions")), result.getString("player_banreason"), result.getString("player_lastserver"), result.getString("player_playtime"), result.getInt("player_banpoints"), result.getString("player_friends"), result.getString("player_friendRequests"), result.getBoolean("player_friendsEnabled"));
                return player;
            } else {
                throw new NullPointerException();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (NullPointerException e) {
            addPlayer(name, uuid);

        }

        return player;
    }

    public synchronized long getLastLogin(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_lastlogin` FROM `players` WHERE `player_uuid` = ?");
            ps.setString(1, uuid);

            ResultSet results = ps.executeQuery();
            boolean hasAccount = results.next();

            if (hasAccount) {
                try {
                    return Long.parseLong(results.getString("player_lastlogin"));

                } catch (NumberFormatException e) {
                    return 0;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public synchronized PermissionSet getPermissions(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_permissions` FROM `players` WHERE `player_uuid` =?");
            ps.setString(1, uuid);

            ResultSet results = ps.executeQuery();
            boolean isAccount = results.next();

            if (isAccount) {
                return PermissionSet.getSetFromPower(results.getInt("player_permissions"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PermissionSet.DEFAULT;
    }

    public synchronized String getLastServer(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_lastserver` FROM `players` WHERE `player_uuid` = ?");
            ps.setString(1, uuid);

            ResultSet results = ps.executeQuery();
            boolean isAccount = results.next();
            if (isAccount) {
                return results.getString("player_lastserver");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized int getElasticBands(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_elasticbands` FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);

            ResultSet results = ps.executeQuery();
            boolean isAccount = results.next();
            if (isAccount) {
                return results.getInt("player_elasticbands");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public synchronized void setElasticBands(String name, int elasticBands) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_elasticbands` = ? WHERE `player_username` = ?");
            ps.setInt(1, elasticBands);
            ps.setString(2, name);
            ps.executeQuery();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized String getNameFromID(int id) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_username` FROM `players` WHERE `player_id` = ?");
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();
            boolean isAccount = results.next();
            if (isAccount) {
                return results.getString("player_username");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public synchronized void savePlayerToDB(ProxiedPlayer player, long playTime) {
        String serverName;
        try {
            serverName = player.getServer().getInfo().getName();
        } catch (NullPointerException e) {
            serverName = null;
        }
        BungeePlayer bp = BungeePlayer.getPlayer(player.getName().toLowerCase());
        if (playTime == -1) {
            if (serverName == null) {
                try {
                    Connection c = DatabaseConnections.getConnection();
                    PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid` =?, `player_lastlogin` = ?, `player_lastip` = ?, `player_friends` = ?, `player_friendsEnabled` = ?, `player_friendRequests` =? WHERE `player_username` = ?");
                    ps.setString(1, player.getUniqueId().toString());
                    ps.setString(2, "" + System.currentTimeMillis() / 1000);
                    ps.setString(3, player.getAddress().getAddress().getHostAddress());
                    ps.setString(4, bp.getFriendString());
                    ps.setBoolean(5, bp.isFriendRequestsEnabled());
                    ps.setString(6, bp.getFriendRequestString());
                    ps.setString(7, player.getName());
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Connection c = DatabaseConnections.getConnection();
                    PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid` =?, `player_lastlogin` = ?, `player_lastserver` = ?, `player_lastip` = ?, `player_friends` = ?, `player_friendsEnabled` = ?, `player_friendRequests` =? WHERE `player_username` = ?");
                    ps.setString(1, player.getUniqueId().toString());
                    ps.setString(2, "" + System.currentTimeMillis() / 1000);
                    ps.setString(3, serverName);
                    ps.setString(4, player.getAddress().getAddress().getHostAddress());
                    ps.setString(5, bp.getFriendString());
                    ps.setBoolean(6, bp.isFriendRequestsEnabled());
                    ps.setString(7, bp.getFriendRequestString());
                    ps.setString(8, player.getName());
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (serverName == null) {
                try {
                    Connection c = DatabaseConnections.getConnection();
                    PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid` =?, `player_lastlogin` = ?, `player_playtime` = ? ,`player_lastip` = ?, `player_friends` = ?, `player_friendsEnabled` = ?, `player_friendRequests` =? WHERE `player_username` = ?");
                    ps.setString(1, player.getUniqueId().toString());
                    ps.setString(2, "" + System.currentTimeMillis() / 1000);
                    ps.setString(3, playTime + "");
                    ps.setString(4, player.getAddress().getAddress().getHostAddress());
                    ps.setString(5, bp.getFriendString());
                    ps.setBoolean(6, bp.isFriendRequestsEnabled());
                    ps.setString(7, bp.getFriendRequestString());
                    ps.setString(8, player.getName());
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Connection c = DatabaseConnections.getConnection();
                    PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid` =?, `player_lastlogin` = ?, `player_playtime` = ? ,`player_lastserver` = ?, `player_lastip` = ?, `player_friends` = ?, `player_friendsEnabled` = ?, `player_friendRequests` =? WHERE `player_username` = ?");
                    ps.setString(1, player.getUniqueId().toString());
                    ps.setString(2, "" + System.currentTimeMillis() / 1000);
                    ps.setString(3, playTime + "");
                    ps.setString(4, serverName);
                    ps.setString(5, player.getAddress().getAddress().getHostAddress());
                    ps.setString(6, bp.getFriendString());
                    ps.setBoolean(7, bp.isFriendRequestsEnabled());
                    ps.setString(8, bp.getFriendRequestString());
                    ps.setString(9, player.getName());
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public synchronized void updateUUIDFromName(String name, String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid`=? WHERE `player_username` = ?");
            ps.setString(1, uuid);
            ps.setString(2, name);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized String[] getBanDataFromName(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);
            ResultSet results = ps.executeQuery();
            if (results.next()) {
                return new String[]{Boolean.toString(results.getBoolean("player_banned")), results.getString("player_banexpiry"), results.getString("player_banreason")};
            }
        } catch (SQLException ex) {
        }

        return null;
    }

    public synchronized String[] getBanDataFromUUID(String name, String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_uuid` = ?");
            ps.setString(1, uuid);
            ResultSet results = ps.executeQuery();
            if (results.next()) {
                return new String[]{Boolean.toString(results.getBoolean("player_banned")), results.getString("player_banexpiry"), results.getString("player_banreason")};
            } else {
                this.getBanDataFromName(name);
            }
        } catch (SQLException ex) {
        }

        return null;
    }

    public synchronized int getPlayerID(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_id` FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("player_id");
            }
        } catch (SQLException ex) {

        }

        return -1;
    }
}
