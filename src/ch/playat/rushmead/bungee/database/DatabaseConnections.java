package ch.playat.rushmead.bungee.database;

import ch.playat.rushmead.bungee.Bungeecord;
import code.husky.mysql.MySQL;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class DatabaseConnections {

    private static MySQL mysql;
    private static Connection c;

    public static Connection getConnection() throws SQLException {
        if( c == null || c.isClosed()){
            openConnection();
        }
        return c;
    }

    public static void openConnection() {
        mysql = new MySQL(Bungeecord.getInstance(), "rushmead.playat.ch", "3306", "elasticmc", "root", "");
        c = mysql.openConnection();
    }

    public static void closeConnection() {

        mysql.closeConnection();
    }

}
