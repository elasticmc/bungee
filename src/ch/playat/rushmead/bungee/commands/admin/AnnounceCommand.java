package ch.playat.rushmead.bungee.commands.admin;

import ch.playat.rushmead.bungee.logs.Log;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class AnnounceCommand extends Command {

    public AnnounceCommand() {
        super("announce");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou don't have permission to use this command.");
            return;
        }
        if (args.length < 1) {
            Messaging.sendMessage(sender, "&cUsage: /announce <message>");
            return;
        }
        String message = "";
        for (String msg : args) {
            message += "&b" + msg + " ";
        }

        Log.ogBroadcasts(sender.getName(), message);
        Messaging.broadcastRawMessage("&6------------------------------------------------");
        Messaging.broadcastRawMessage("&e&lANNOUNCEMENT&r&e: &b" + Messaging.colorizeMessage(message));
        Messaging.broadcastRawMessage("&6------------------------------------------------");

    }

}
