package ch.playat.rushmead.bungee.commands.admin;

import ch.playat.rushmead.bungee.database.DatabaseConnections;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import ch.playat.rushmead.bungee.ranks.Ranks;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class WhoIsCommand extends Command {

    public WhoIsCommand() {
        super("whois");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou don't have permission to use this command.");
            return;
        }
        if (args.length == 0) {
            Messaging.sendMessage(sender, "&cUsage: /whois <player>");
            return;
        }

        try {
            ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(args[0]);
            BungeePlayer target = BungeePlayer.getPlayer(args[0].toLowerCase());
            
            String serverName = targetPlayer.getServer().getInfo().getName();
            long playSecs = target.getPlayTime();
            String playTime = "";

            if (playSecs > 60 * 24) {
                long days = playSecs / 60 / 60 / 24;
                playTime = days + "d ";
                playSecs -= days * 60 * 60 * 24;
            }
            if (playSecs > 60 * 60) {
                long hours = playSecs / 60 / 60;
                playTime = hours + "h ";
                playSecs -= hours * 60 * 60;
            }
            if (playSecs > 60) {
                long mins = playSecs / 60;
                playTime = mins + "m ";
                playSecs -= mins * 60;
            }
            if (playSecs > 0) {
                playTime = playSecs + "s";
            }

            Messaging.sendMessage(sender, "&6Information for {0} &a(Online)", target.getName());
            Messaging.sendMessage(sender, "&6Current server: &b" + targetPlayer.getServer().getInfo().getName());
            Messaging.sendMessage(sender, "&6Play time: " + playTime);
            Messaging.sendMessage(sender, "&6Rank: " + target.getRank().getColor() + target.getRank().getName());
            if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
            } else {
                if (target.getRank().getPermissionSet().getPower() != target.getPermissions().getPower()) {
                    Messaging.sendMessage(sender, "&6Custom permissions: &e" + target.getPermissions().name());
                }
                if(sender instanceof ProxiedPlayer && target.getPermissions().getPower() < BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower()){
                             Messaging.sendMessage(sender, "&6IP: &e" + targetPlayer.getAddress().getAddress().getHostAddress());
                }
       
            }
            Messaging.sendMessage(sender, "&6Elastic Bands: &e" + target.getElasticBands());
            Messaging.sendMessage(sender, "&6Not banned.");
            Messaging.sendMessage(sender, "&6Ban points: &e" + target.getBanPoints());

            return;
        } catch (NullPointerException ex) {
        }

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `username` = ?");
            ps.setString(1, args[0]);

            ResultSet rs = ps.executeQuery();
            boolean has = rs.next();

            if (has) {
                Messaging.sendMessage(sender, "&6Information for {0} &c(Offline)", rs.getString("username"));
                Messaging.sendMessage(sender, "&6Rank: " + Ranks.getRanks().getRankFromID(rs.getInt("rank")).getName());
                if (!rs.getString("rankexpiry").equalsIgnoreCase("0")) {
                    Messaging.sendMessage(sender, "&6Rank expiry: &e" + new Timestamp(Long.parseLong(rs.getString("rankexpiry")) * 1000).toString());
                }
                if (Ranks.getRanks().getRankFromID(rs.getInt("rank")).getPermissionSet() != PermissionSet.getSetFromPower(rs.getInt("permissions"))) {
                    Messaging.sendMessage(sender, "&6Custom permissions: &e" + PermissionSet.getSetFromPower(rs.getInt("permissions")).name());
                }
                Messaging.sendMessage(sender, "&6Last seen on: &e" + rs.getString("lastserver"));
                Messaging.sendMessage(sender, "&6Last known IP: &e" + rs.getString("lastip"));
                Messaging.sendMessage(sender, "&6Tokens: &e" + rs.getInt("tokens"));

                if (rs.getBoolean("banned")) {
                    if (rs.getString("banexpiry") != "0") {
                        Messaging.sendMessage(sender, "&6Banned until: &e" + new Timestamp(Long.parseLong(rs.getString("banexpiry")) * 1000).toString());
                        Messaging.sendMessage(sender, "&6Ban reason: &e" + rs.getString("banreason"));
                    }
                } else {
                    Messaging.sendMessage(sender, "&6Not banned.");
                }
                Messaging.sendMessage(sender, "&6Ban points: &e" + rs.getInt("banpoints"));
            } else {
                throw new NullPointerException();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            Messaging.sendMessage(sender, "&cThe requested player cannot be found.");
            return;
        }
    }

}
