package ch.playat.rushmead.bungee.commands.admin;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Rushmead for ElasticMC
 */
public class TogglePublic extends Command {

    public TogglePublic() {
        super("public");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            if (BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
                Messaging.sendMessage(sender, "&4You do not have permission to run this command.");
                return;
            } else {
                if (Bungeecord.getInstance().isPublic) {
                    Bungeecord.getInstance().isPublic = false;
                    Messaging.sendMessage(sender, "&aPublic mode is toggled &cOff");
                } else {
                    Bungeecord.getInstance().isPublic = true;
                    Messaging.sendMessage(sender, "&aPublic mode is toggled &2On");
                }
            }
        } else {
            if (Bungeecord.getInstance().isPublic) {
                Bungeecord.getInstance().isPublic = false;
                Messaging.sendMessage(sender, "&aPublic mode is toggled &cOff");
            } else {
                Bungeecord.getInstance().isPublic = true;
                Messaging.sendMessage(sender, "&aPublic mode is toggled &2On");
            }
        }
    }
}
