package ch.playat.rushmead.bungee.commands.tp;

import ch.playat.rushmead.bungee.messaging.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class HubCommand extends Command {

    public HubCommand() {
        super("hub");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            Messaging.sendMessage(sender, "&cOnly players can use this command.");
            return;
        }
        ProxiedPlayer p = (ProxiedPlayer) sender;

        if (args.length == 0) {
            Messaging.sendMessage(sender, "&3You're being sent to Hub{0}", 1);
            p.connect(ProxyServer.getInstance().getServerInfo("hub1"));
        } else {
            Messaging.sendMessage(sender, "&cUsage: /hub");
        }
    }
}
