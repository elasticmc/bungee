package ch.playat.rushmead.bungee.commands.mod;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class StaffCommand extends Command {

    public StaffCommand() {
        super("staff");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.JNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou don't have permission to use this command.");
            return;
        }
        if (args.length < 1) {
            Messaging.sendMessage(sender, "&cUsage: /staff <message>");
            return;
        }
        String message = "";
        for (String s : args) {
            message += s + " ";
        }
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            Messaging.sendStaffMessage(ChatColor.YELLOW + player.getServer().getInfo().getName() + " &8\u23aa " + BungeePlayer.getPlayer(sender.getName().toLowerCase()).getRank().getPrefix() + sender.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + message);
            Bungeecord.getInstance().session.sendMessage(Bungeecord.getInstance().session.findChannelByName("staffchat"), BungeePlayer.getPlayer(sender.getName().toLowerCase()).getRank().getName() + "| " + player.getName() + ": " + message, null);
        } else {

            Messaging.sendStaffMessage(ChatColor.YELLOW + "Bungee &8\u23aa " + ChatColor.DARK_AQUA + sender.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + message);
            Bungeecord.getInstance().session.sendMessage(Bungeecord.getInstance().session.findChannelByName("staffchat"), "Bungee: " + message, null);
        }

    }

}
