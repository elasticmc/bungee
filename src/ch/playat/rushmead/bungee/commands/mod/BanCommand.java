package ch.playat.rushmead.bungee.commands.mod;

import ch.playat.rushmead.bungee.bans.Bans;
import ch.playat.rushmead.bungee.bans.Offense;
import ch.playat.rushmead.bungee.bans.UsefullBanStuff;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.players.BungeePlayerDB;
import ch.playat.rushmead.bungee.ranks.PermissionSet;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class BanCommand extends Command {

    public BanCommand() {
        super("ban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < PermissionSet.JNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou don't have permission to use this command.");
            return;
        }
  

        if (args.length == 2 && !args[0].equalsIgnoreCase("help")) {
                    if (args.length > 0  && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() <       BungeePlayerDB.getDatas().getPlayer(args[0].toLowerCase(), "").getPermissions().getPower()) {
            Messaging.sendMessage(sender, "&cYou cannot ban this player.");
            return;
        }
            Offense offense = Offense.getOffense(args[1]);

            if (offense == null) {
                Messaging.sendMessage(sender, "&cOffense ban not found.");
                Messaging.sendMessage(sender, "&cUse comand /ban help for more infomation");
                return;
            }
            if (sender instanceof ProxiedPlayer && BungeePlayer.getPlayer(sender.getName().toLowerCase()).getPermissions().getPower() < offense.getMinimumPermissions().getPower()) {
                Messaging.sendMessage(sender, "&cYou do not have permission to ban for this offense.");
                return;
            }
            if (!Bans.getBanDB().hasBanExpired(args[0])) {
                Messaging.sendMessage(sender, "&cThat player is already banned.");
                return;
            }

            Bans.getBanDB().banPlayer(sender.getName(), args[0], offense, "", false);
            try {
                ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
                target.disconnect(UsefullBanStuff.kickMessage(0, offense.getName()));
            } catch (NullPointerException ex) {

            }
            Messaging.sendStaffMessage("&c&l{0} &chas been banned for {1} by {2}", args[0], offense.getName(), sender.getName());
        } else if (args.length >= 1 && args[0].equalsIgnoreCase("help")) {
            Messaging.sendRawMessage(sender, "&8---- &6Banning Ofenses &8----");
            Messaging.sendRawMessage(sender, "&8- &6Ban Evasion &8-");
            Messaging.sendRawMessage(sender, "&8- &6Spam &8-");
        } else {
            Messaging.sendMessage(sender, "&cUsage: /ban <player> <reason>");
        }
    }

}
