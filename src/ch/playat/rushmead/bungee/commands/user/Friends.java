package ch.playat.rushmead.bungee.commands.user;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import ch.playat.rushmead.bungee.ranks.Ranks;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author Rushmead
 */
public class Friends extends Command {

    public Friends() {
        super("friends");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            Messaging.sendMessage(sender, "&cIncorrect usage: /friends add/remove/list/accept/deny/join/toggle");
            return;
        }
        if (!(sender instanceof ProxiedPlayer)) {
            Messaging.sendMessage(sender, "&cPlayer Only Command.");
            return;
        }
        String subcommand = args[0];
        String subcommand1 = "";
        if (args.length > 1) {
            subcommand1 = args[1];
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        switch (subcommand) {
            case "add":
                if (Bungeecord.getInstance().getProxy().getPlayer(subcommand1) != null) {
                    if (BungeePlayer.getPlayer(subcommand1.toLowerCase()).isFriendRequestsEnabled()) {
                        if (BungeePlayer.getPlayer(subcommand1.toLowerCase()).getFriends().contains(player.getDisplayName())) {
                            Messaging.sendMessage(player, "&4You are already friends with {0}", subcommand1);
                        } else {
                            if (BungeePlayer.getPlayer(subcommand1.toLowerCase()).getFriendRequests().contains(player.getDisplayName())) {
                                Messaging.sendMessage(player, "&4You already have sent that player a friend request");
                            } else {
                                BungeePlayer.getPlayer(subcommand1.toLowerCase()).getFriendRequests().add(player.getName());
                                Messaging.sendMessage(player, "&5Sent friend Request to {0}", subcommand1);
                                Messaging.sendMessage(Bungeecord.getInstance().getProxy().getPlayer(subcommand1), "&5&l{0} &6has sent you a friend Request!", player.getDisplayName());
                            }
                        }
                    } else {
                        Messaging.sendMessage(player, "&4That player does not have Friend Requests Enabled.");
                    }
                } else {
                    Messaging.sendMessage(player, "&4Could not find player with that username.");
                }
                break;
            case "accept":
                if (BungeePlayer.getPlayer(player.getDisplayName().toLowerCase()).getFriendRequests().contains(subcommand1)) {
                    BungeePlayer.getPlayer(player.getDisplayName().toLowerCase()).getFriendRequests().remove(subcommand1);
                    BungeePlayer.getPlayer(subcommand1.toLowerCase()).getFriends().add(player.getDisplayName());
                    BungeePlayer.getPlayer(player.getDisplayName().toLowerCase()).getFriends().add(subcommand1);
                    Messaging.sendMessage(player, "&5{0} is now your friend", subcommand1);
                } else {
                    Messaging.sendMessage(player, "&4You do not have a friend request from that player.");
                }
                break;
            case "deny":
                if (BungeePlayer.getPlayer(player.getDisplayName().toLowerCase()).getFriendRequests().contains(subcommand1)) {
                    BungeePlayer.getPlayer(player.getDisplayName().toLowerCase()).getFriendRequests().remove(subcommand1);
                    Messaging.sendMessage(player, "&5Denied friend request from {0}, they will not be notified", subcommand1);
                } else {
                    Messaging.sendMessage(player, "&4You do not have a friend request from that player.");
                }
                break;
            case "remove":
                if (!BungeePlayer.getPlayer(player.getName().toLowerCase()).getFriends().contains(subcommand1)) {
                    Messaging.sendMessage(player, "&4" + subcommand1 + " is not on your friends list");
                } else if (BungeePlayer.getPlayer(player.getName().toLowerCase()).getFriends().contains(subcommand1)) {
                    BungeePlayer.getPlayer(player.getName().toLowerCase()).getFriends().remove(subcommand1);
                    Messaging.sendMessage(player, "&5" + subcommand1 + " is no longer you friend");
                }
                break;
            case "list":
                if (BungeePlayer.getPlayer(player.getName().toLowerCase()).getFriends().isEmpty()) {
                    Messaging.sendMessage(player, "&4You don't have any friends");
                } else {
                    Messaging.sendMessage(player, "&5Friends List");
                    for (String s : BungeePlayer.getPlayer(player.getName().toLowerCase()).getFriends()) {
                        if (s.isEmpty()) {
                            continue;
                        }
                        if (Bungeecord.getInstance().getProxy().getPlayer(s) == null) {
                            Messaging.sendRawMessage(player, ChatColor.GRAY + "- {0}{1} {2} - {3}", Ranks.getRanks().getRankFromDatabase(s).getColor(), s, ChatColor.GRAY, ChatColor.RED + "Offline");
                        } else {
                            Messaging.sendRawMessage(player, ChatColor.GRAY + "- {0}{1} {2} - {3}", Ranks.getRanks().getRankFromDatabase(s).getColor(), s, ChatColor.GRAY, ChatColor.GREEN + "Online");
                        }

                    }
                }
                break;
            case "toggle":
                if (BungeePlayer.getPlayer(player.getName().toLowerCase()).isFriendRequestsEnabled()) {
                    BungeePlayer.getPlayer(player.getName().toLowerCase()).setFriendRequestsEnabled(false);
                    Messaging.sendMessage(sender, "&aToggled Friend Requests off");
                } else {
                    BungeePlayer.getPlayer(player.getName().toLowerCase()).setFriendRequestsEnabled(true);
                    Messaging.sendMessage(sender, "&aToggled Friend Requests on");
                }
        }

    }

}
