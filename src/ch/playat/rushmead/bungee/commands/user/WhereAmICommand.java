package ch.playat.rushmead.bungee.commands.user;

import ch.playat.rushmead.bungee.messaging.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Rushmead
 */
public class WhereAmICommand extends Command {

    public WhereAmICommand() {
        super("whereami");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            Messaging.sendMessage(sender, "&cOnly players can use this command.");
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        Messaging.sendMessage(sender, "&6You're currently playing on &b" + player.getServer().getInfo().getName());
    }

}
