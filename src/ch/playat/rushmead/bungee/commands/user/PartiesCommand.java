package ch.playat.rushmead.bungee.commands.user;

import ch.playat.rushmead.bungee.Bungeecord;
import ch.playat.rushmead.bungee.messaging.Messaging;
import ch.playat.rushmead.bungee.partys.Party;
import ch.playat.rushmead.bungee.partys.PartyManager;
import ch.playat.rushmead.bungee.players.BungeePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Rushmead for ElasticMC
 */
public class PartiesCommand extends Command {

    public PartiesCommand() {
        super("party", null, "squad");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            Messaging.sendMessage(sender, "&a /party invite <username> - Invite a player(Makes new party if your not in one.)(Party Leader)");
            Messaging.sendMessage(sender, "&a /party remove <username> - remove a player(Party Leader)");
            Messaging.sendMessage(sender, "&a /party leave- Leave the party");
            Messaging.sendMessage(sender, "&a /party warp - Warps all party players to your party(Party Leader)");
            Messaging.sendMessage(sender, "&a /party accept <username> - Accept a party invite");
            Messaging.sendMessage(sender, "&a /party list - List all players in Party");
            return;
        }
        if (!(sender instanceof ProxiedPlayer)) {
            Messaging.sendMessage(sender, "&a This command can only be run by Players.");
            return;
        }
        BungeePlayer bp = BungeePlayer.getPlayer(sender.getName().toLowerCase());
        switch (args[0]) {
            case "invite":
                if (bp.isInParty()) {
                    if (bp.getParty().getPartyLeader().toLowerCase().equals(bp.getName().toLowerCase())) {
                        if (Bungeecord.getInstance().getProxy().getPlayer(args[1]) != null) {
                            if (bp.getParty().getPartyInvites().contains(args[1].toLowerCase())) {
                                Messaging.sendMessage(sender, "&4You have already invited that user.");
                            } else if (bp.getParty().getPartyPlayers().contains(args[1])) {
                                Messaging.sendMessage(sender, "&4That player is already in your party.");
                            } else {
                                Messaging.sendMessage(sender, "&aParty invite sent to &6{0}", args[1]);
                                bp.getParty().addPartyInvite(args[1].toLowerCase());
                                Messaging.sendClickableAndHoveringMessage(Bungeecord.getInstance().getProxy().getPlayer(args[1]), "&aYou have been invited to a party by &6" + bp.getName(), "&aClick to accept", "/party accept " + sender.getName().toLowerCase());
                            }
                        } else {
                            Messaging.sendMessage(sender, "&4Could not find that player.");
                        }
                    } else {
                        Messaging.sendMessage(sender, "&4You are not the party leader.");
                    }
                } else {
                    Party p = PartyManager.newParty(sender.getName().toLowerCase());
                    bp.setParty(p);
                    if (bp.getParty().getPartyLeader().toLowerCase().equals(bp.getName().toLowerCase())) {
                        if (Bungeecord.getInstance().getProxy().getPlayer(args[1]) != null) {
                            if (bp.getParty().getPartyInvites().contains(args[1])) {
                                Messaging.sendMessage(sender, "&4You have already invited that user.");
                            } else if (bp.getParty().getPartyPlayers().contains(args[1])) {
                                Messaging.sendMessage(sender, "&4That player is already in your party.");
                            } else {
                                Messaging.sendMessage(sender, "&aParty invite sent to {0}", args[1]);
                                bp.getParty().addPartyInvite(args[1].toLowerCase());
                                Messaging.sendClickableAndHoveringMessage(Bungeecord.getInstance().getProxy().getPlayer(args[1]), "&aYou have been invited to a party by &6" + bp.getName(), "&aClick to accept", "/party accept " + sender.getName().toLowerCase());
                            }
                        } else {
                            Messaging.sendMessage(sender, "&4Could not find that player.");
                        }
                    } else {
                        Messaging.sendMessage(sender, "&4You are not the party leader.");
                    }
                }

                break;
            case "remove":
                if (bp.isInParty()) {
                    if (bp.getParty().getPartyLeader().toLowerCase().equals(bp.getName().toLowerCase())) {
                        if (Bungeecord.getInstance().getProxy().getPlayer(args[1]) != null) {
                            if (bp.getParty().getPartyInvites().contains(args[1].toLowerCase())) {
                                Messaging.sendMessage(sender, "&4Canceled invite to {0}", args[1]);
                                bp.getParty().getPartyInvites().remove(args[1].toLowerCase());
                            } else if (bp.getParty().getPartyPlayers().contains(args[1])) {
                                Messaging.sendMessage(sender, "&aRemoved {0} from party", args[1]);
                                bp.getParty().getPartyPlayers().remove(args[1].toLowerCase());
                                Messaging.sendPartyMessage(bp.getParty(), "&a{0} have left the party", args[1]);
                            } else {
                                Messaging.sendMessage(sender, "&4That player is not in your party");
                            }
                        } else {
                            Messaging.sendMessage(sender, "&4Could not find that player.");
                        }
                    } else {
                        Messaging.sendMessage(sender, "&4You are not the party leader.");
                    }
                } else {
                    Messaging.sendMessage(sender, "&4You are not in a party.");
                }

                break;
            case "accept":
                if (!bp.isInParty()) {
                    if (PartyManager.getParties().containsKey(args[1].toLowerCase())) {
                        if (PartyManager.getParties().get(args[1].toLowerCase()).getPartyInvites().contains(bp.getName().toLowerCase())) {
                            bp.setParty(PartyManager.getParties().get(args[1]));
                            bp.getParty().getPartyInvites().remove(bp.getName().toLowerCase());
                            bp.getParty().getPartyPlayers().add(bp.getName().toLowerCase());
                            Messaging.sendPartyMessage(bp.getParty(), "&6{0} &ahas joined the party!", bp.getName());
                        } else {
                            Messaging.sendMessage(sender, "&4You dont have a party invite from that user.");
                        }
                    } else {
                        Messaging.sendMessage(sender, "&4You dont have a party invite from that user.");
                    }

                } else {
                    Messaging.sendMessage(sender, "&4Your already in a party.");
                }
                break;
            case "leave":
                if (bp.isInParty()) {
                    bp.getParty().getPartyPlayers().remove(bp.getName().toLowerCase());
                    Messaging.sendPartyMessage(bp.getParty(), "&6{0} &ahas left the party", bp.getName());
                    bp.setParty(null);
                } else {
                    Messaging.sendMessage(sender, "&4You are not in a party.");
                }
                break;
            case "list":
                if (bp.isInParty()) {
                    Messaging.sendRawMessage(sender, "&aParty - Leader: &6{0}", bp.getParty().getPartyLeader());
                    for (String s : bp.getParty().getPartyPlayers()) {
                        Messaging.sendRawMessage(sender, "&7-&6{0}", s);
                    }
                } else {
                    Messaging.sendMessage(sender, "&4You are not in a party.");
                }
                break;
            case "warp":
                if (bp.isInParty()) {
                    if (bp.getParty().getPartyLeader().equals(bp.getName().toLowerCase())) {
                        ServerInfo si = Bungeecord.getInstance().getProxy().getPlayer(bp.getName().toLowerCase()).getServer().getInfo();
                        Messaging.sendPartyMessage(bp.getParty(), "&aParty Leader summoned you to their server.");
                        for (String s : bp.getParty().getPartyPlayers()) {
                            ProxiedPlayer pp = Bungeecord.getInstance().getProxy().getPlayer(s.toLowerCase());
                            pp.connect(si);
                        }
                    } else {
                        Messaging.sendMessage(sender, "&4You are not the party leader.");
                    }
                } else {
                    Messaging.sendMessage(sender, "&4You are not in a party.");
                }
                break;
        }
    }

}